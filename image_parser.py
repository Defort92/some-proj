import csv

from gspread.exceptions import APIError
from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, JavascriptException
from selenium.webdriver.chrome.options import Options
from time import sleep, time


#  PARAMS  --------------------------------------------------------------------------------------------


ROW = [
    "Наименование товара",
    "Адрес",
    "Имеется ли товар в наличии",
]
MAX_ROWS = 5000
HOST = "https://sbermegamarket.ru/"


#  main block  ----------------------------------------------------------------------------------------


def main(host):
    option = Options()

    option.add_argument("--disable-infobars")
    option.add_argument("start-maximized")
    option.add_argument("--disable-extensions")

    # Pass the argument 1 to allow and 2 to block
    option.add_experimental_option(
        "prefs", {"profile.default_content_setting_values.notifications": 1}
    )

    browser = Chrome(
        chrome_options=option, executable_path=".\\chromedriver.exe"
    )
    start = time()

    first_start(browser, host)
    with open("links.txt", encoding="utf-8") as links, \
            open("data.csv", "w") as data, \
            open("errors.csv", "w") as error:
        for link in links:
            link = link.strip()
            browser.get(link)
            writer_data = csv.writer(data, delimiter="|", lineterminator="\n",)
            writer_data.writerow(ROW)
            writer_error = csv.writer(error, delimiter="|", lineterminator="\n",)
            writer_error.writerow(ROW)

            with open("locations.txt", encoding="utf-8") as locations:
                for location in locations:
                    location = location.strip()
                    try:
                        data = _get_info(browser, location)
                        writer_data.writerow((data["name"], data["location"], data["is_present"]))
                        print(data)
                    except Exception as e:
                        writer_error.writerow(("---", location, "no"))
                        print("---", location, "no")
                        browser.get(link)

    browser.close()
    print("time: ", time() - start)


def first_start(browser, host):
    browser.get(host)
    sleep(15)
    script = """
       var first = document.getElementsByClassName("flocktory-widget-overlay")[0];
       var second = document.getElementsByClassName("flocktory-widget-overlay")[1];
       first.parentNode.removeChild(first);
       second.parentNode.removeChild(second);
    """
    # - useless stuff
    try:
        browser.execute_script(script)
    except JavascriptException as e:
        print(e)

    modal = get_element_by_xpath(
        browser, '//*[@id="page-header"]/div/div[2]/div/div[2]/div/div/div/div/div/div[3]/button[1]'
    )
    if modal:
        modal.click()


def _get_info(browser, current_location):
    """ 2_|__ пример: https://sbermegamarket.ru/catalog/details/starbucks..."""
    delete_previous_location(browser)
    change_location(browser, current_location)
    all_data = {
        "location": current_location,
        "name": get_element_by_xpath(browser, '//*[@id="app"]/main/div/div[1]/div[3]/article/div[1]/header/h1').text,
        "is_present": "no",
    }
    sleep(1.5)
    data_block = get_element_by_class_name(browser, "prod-finished", delay=2)
    if data_block:
        all_data["is_present"] = "no"
        return all_data

    button = get_element_by_class_name(browser, "subscribe-button__btn", delay=2)
    if button:
        all_data["is_present"] = "no"
        return all_data

    data_block = get_element_by_class_name(browser, "pdp-sales-block__button", delay=1)
    if data_block:
        all_data["is_present"] = "YES"
        return all_data


def change_location(browser, location_name: str):
    check_on_useless_windows(browser)
    open_address = "header-user-address-button__caret"
    open_menu = get_element_by_class_name(browser, open_address, delay=1)
    open_menu.click()

    button_classname = "header-region-selector-view__address-block-button"
    get_element_by_class_name(browser, button_classname, delay=1).click()

    search_field_classname = "profile-address-create__search"
    search = get_element_by_class_name(browser, search_field_classname)
    search_field = search.find_elements(by=By.CLASS_NAME, value="search-field-input")[0]
    search_field.clear()
    search_field.send_keys(location_name)
    sleep(2)

    # click on first suggestion
    search.find_elements(by=By.CLASS_NAME, value="suggestions__item")[0].click()
    # confirm  profile-address-create__search-btn
    get_element_by_class_name(browser, "profile-address-create__search-btn").click()
    # close menu
    open_menu = get_element_by_class_name(browser, open_address, delay=1)
    sleep(0.7)
    open_menu.click()


def delete_previous_location(browser):
    check_on_useless_windows(browser)
    class_name = "address-selector-list-item__icon"
    open_address = "header-user-address-button__caret"

    open_menu = get_element_by_class_name(browser, open_address, delay=1)
    open_menu.click()

    delete_button = get_element_by_class_name(browser, class_name, delay=1)
    if not delete_button:
        # close menu
        open_menu = get_element_by_class_name(browser, open_address, delay=0)
        open_menu.click()
        return

    delete_button.click()
    sleep(1.5)
    class_name = "delete-address-modal__footer"
    delete_button = get_element_by_class_name(browser, class_name, delay=0)
    delete_button.find_elements(by=By.TAG_NAME, value="button")[0].click()
    # close menu
    open_menu = get_element_by_class_name(browser, open_address, delay=3)
    open_menu.click()


def check_on_useless_windows(browser):
    modal = get_element_by_xpath(
        browser, '//*[@id="page-header"]/div/div[2]/div/div[2]/div/div/div/div/div/div[3]/button[1]'
    )
    if modal:
        modal.click()
    another_one = get_element_by_xpath(
        browser, '//*[@id="page-header"]/div/div[2]/div/div[2]/div/div/div/div/div/button'
    )
    if another_one:
        another_one.click()


#  inner functions  -----------------------------------------------------------------------------------


def get_element_by_xpath(browser, xpath: str, delay: int = 3):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.XPATH, xpath)))
    except TimeoutException:
        pass
    return some_element


def get_element_by_class_name(browser, class_name: str, delay: int = 3):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.CLASS_NAME, class_name)))
    except TimeoutException:
        pass
    return some_element


def get_elements_by_class_name(browser, class_name: str, delay: int = 2):
    some_elements = None
    try:
        Wait(browser, delay).until(ec.presence_of_element_located((By.CLASS_NAME, class_name)))
        some_elements = browser.find_elements(by=By.CLASS_NAME, value=class_name)
    except TimeoutException:
        pass
    return some_elements


def get_element_by_id(browser, id: str, delay: int = 2):
    some_element = None
    try:
        some_element = Wait(browser, delay).until(ec.presence_of_element_located((By.ID, id)))
    except TimeoutException:
        pass
    return some_element


#  testing/start block  -------------------------------------------------------------------------------


if __name__ == "__main__":
    main(HOST)
